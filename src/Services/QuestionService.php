<?php


namespace Ucc\Services;


use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;

    }

    private function loadQuestions(): Array  {
        // Todo: Add JSON validator
        return $this->jsonMapper->mapArray($this->json->decodeFile(self::QUESTIONS_PATH), [], new Question());
    }

    private function getRandomValue(int $max): int {
        return random_int(0, $max);
    }

    public function getRandomQuestions(int $count = 5): array
    {
        $questions = $this->loadQuestions();
        $totalOfQuestions = count($questions)-1;
        if ($count <= 0) {
            throw new Exception('At lease one question need to be selected on random mode');
        }
        if ($count > $totalOfQuestions) {
            throw new Exception('Number of random questions requested is bigger than the number of available questions');
        }
        $randomizedQuestions = array_rand($questions, $count);
        if ($count === 1) {
            $questionsToReturn = [$questions[$randomizedQuestions]];
        } else {
            $questionsToReturn = [];
            foreach ($randomizedQuestions as $rand_question) {
                $questionsToReturn[] = $questions[$rand_question];
            }
        }
        return $questionsToReturn;
    }

    public function getQuestion(int $id): ?Question {
        $questions = $this->loadQuestions();
        foreach ($questions as $question) {
            if ($question->id === $id) {
                return $question;
            }
        }
    }

    public function getPointsForAnswer(int $id, string $answer): int
    {
        $questions = $this->loadQuestions();
        $questionFiltered = array_filter($questions, function($el) use ($id) {
           return $el->getId() === $id;
        }) ?? null;
        $question = array_shift($questionFiltered);
        if ($question->getCorrectAnswer() === $answer) {
            return $question->getPoints();
        }
        return 0;
    }
}