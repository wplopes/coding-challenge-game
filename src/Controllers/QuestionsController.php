<?php


namespace Ucc\Controllers;


use Ucc\Http\JsonResponseTrait;
use Ucc\Services\QuestionService;
use Ucc\Session;

class QuestionsController
{
    use JsonResponseTrait;
    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        $this->questionService = $questionService;
        $this->requestBody = json_decode(file_get_contents('php://input'));
    }

    public function beginGame(): bool
    {
        $name = $this->requestBody->name ?? null;
        if (empty($name)) {
            $this->json('You must provide a name', 400);
            return false;
        }

        Session::set('name', $name);
        Session::set('questionCount', 1);
        $question = $this->questionService->getRandomQuestions(1);
        $this->json(['question' => $question], 201);
        return true;
    }

    public function answerQuestion(int $id): bool {
        if ( Session::get('name') === null ) {
            $this->json('You must first begin a game', 400);
            return false;
        }

        if ((int)Session::get('questionCount') > 4) {
            $name = Session::get('name');
            $points = Session::get('points');
            Session::destroy();
            $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
            return true;
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            $this->json('You must provide an answer', 400);
            return false;
        }

        $pointsEarned = $this->questionService->getPointsForAnswer($id, $answer);
        if ($pointsEarned > 0) {
            $message = "Right choice! You've earned {$pointsEarned} points...";
            $points = Session::get('points');
            $totalPoints = $points + $pointsEarned;
            Session::set('points', $totalPoints);
        } else {
            $message = "Wrong choice, better luck in the next answer!";
        }
        $questionCount = Session::get('questionCount');
        Session::set('questionCount', ++$questionCount);
        $question = $this->questionService->getRandomQuestions(1, );

        $this->json(['message' => $message, 'question' => $question], 200);
        return true;
    }
}